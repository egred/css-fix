var classes = {
  'BRANCH_BTN_ACTIVE': 'v-members__list-item--active',
  'BRANCH_INFO': 'v-members__list-branch',
  'BRANCH_INFO_ACTIVE': 'v-members__list-branch--active',
  'SUBCATEGORY': 'v-members__subcategories-items',
  'SUBCATEGORY_ACTIVE': 'v-members__subcategories-items--active',
  'SUBCATEGORY_BTN': 'v-members__subcategories-item',
  'SUBCATEGORY_BTN_ACTIVE': 'v-members__subcategories-item--active'
}

$(document).ready(function () {

  $('.js-branch-btn').click(function (evt) {
    showBranchInfo(evt)
  })

  $('.js-close-subcategory').click(function (evt) {
    showBranchInfo(evt)
  })

  $('.v-members__subcategories-item').click(function (evt) {
    showSubcategory(evt)
  })

  $('.js-subcategories-btnm').click(function (evt) {
    showSubcategories(evt)
  })
})




function showBranchInfo(event) {
  var $btn = $(event.currentTarget);
  this.activateBtn($btn, this.classes.BRANCH_BTN_ACTIVE);
  $('body').find('.' + this.classes.SUBCATEGORY_ACTIVE).removeClass(this.classes.SUBCATEGORY_ACTIVE);
  $('body').find('.' + this.classes.SUBCATEGORY_BTN_ACTIVE).removeClass(this.classes.SUBCATEGORY_BTN_ACTIVE);
  this.showInfo($btn.data('branch-id'), this.classes.BRANCH_INFO, this.classes.BRANCH_INFO_ACTIVE);
}

closeSubcategory: function closeSubcategory(e) {
  var $btn = $(e.currentTarget);
  $btn.parents('.' + this.classes.SUBCATEGORY_ACTIVE).removeClass(this.classes.SUBCATEGORY_ACTIVE);
  this.$el.find('.' + this.classes.SUBCATEGORY_BTN_ACTIVE).removeClass(this.classes.SUBCATEGORY_BTN_ACTIVE);
}

showSubcategory: function showSubcategory(e) {
  var $btn = $(e.currentTarget);
  this.activateBtn($btn, this.classes.SUBCATEGORY_BTN_ACTIVE);
  var subcategoryId = $btn.data('subcategory-id');
  this.showInfo(subcategoryId, this.classes.SUBCATEGORY, this.classes.SUBCATEGORY_ACTIVE);
}

showSubcategories: function showSubcategories(e) {
  var $btn = $(e.currentTarget);
  var $btnContainer = $btn.parents('.js-subcategories-btn-container');
  if (!$btnContainer)
    throw '.js-subcategories-btn-container element is required as parent of clicked one';
  var subcategoriesIdToShow = $btnContainer.data('subcategories-id');
  if (!subcategoriesIdToShow)
    throw 'data-subcategories-id is required as attribute of .js-subcategories-btn-container';
  //activate container
  this.$el.find('.js-subcategories-btn-container').addClass('v-members__categories-item--inactive');
  $btnContainer.removeClass('v-members__categories-item--inactive');
  //show active subcategories and hide others
  this.$el.find('.js-subcategories').addClass('v-members__subcategories--inactive');
  this.$el.find('#' + subcategoriesIdToShow).removeClass('v-members__subcategories--inactive');
}

activateBtn: function activateBtn($btn, activeClass) {
  $btn.siblings('.' + activeClass).removeClass(activeClass);
  $btn.addClass(activeClass);
}

showInfo: function showInfo(infoElementId, elementClass, elementActiveClass) {
  $('body').find('.' + elementClass).removeClass(elementActiveClass);
  $('body').find('#' + infoElementId).addClass(elementActiveClass);
}


