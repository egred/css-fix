

$(document).ready(function () {
  $('.slider-imgs').slick({
    fade: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-txts',
    cssEase: 'ease-in',
    autoplay: true,
    autoplaySpeed: 30000    
  });
  $('.slider-txts').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.slider-prev'),
    nextArrow: $('.slider-next'),
    asNavFor: '.slider-imgs',
    autoplay: true,
    autoplaySpeed: 30000    
  });
  $('.slider-imgs').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var svg = $($('.slider-imgs .slide')[nextSlide]).find('.slide-svg img')[0];
    if(svg)svg.src = svg.src + '?random='+new Date().getTime();
});
  
  initDevYears()

  /*
   $('body').click(function () {
   clearDevEvents()
   })
   */

});

function initDevYears() {
  $('.dev-year').each(function () {
    var year = this.attributes.year.value
    var container = $(this).find('div.dev-events-front-container')[0]
    getDevYear(year, container)
  });
}

function getDevYear(year, container) {
  $.ajax({
    // beforeSend: setLoading(container, true),
    // complete: setLoading(container, false),
    url: getAppUri() + '/development-events-front?year=01.01.' + year,
    // method: "POST",
    context: container,
  }).done(function (result) {
    $(container).html(result)
    initDevYearEvents(container)
  });
}

function initDevYearEvents(container) {
  $(container).find('ul > li').each(function (index, el) {
    $(el).click(function () {
      clearDevEvents()
      var date = el.attributes.date.value
      getDevEvents(date, el)
    })
  })
}

function getDevEvents(date, container) {
  $.ajax({
    // beforeSend: setLoading(container, true),
    // complete: setLoading(container, false),
    url: getAppUri() + '/development-events-popup?date=' + date,
    // method: "POST",
    context: container,
  }).done(function (result) {
    var dPop = document.createElement('div')
    dPop.className = 'dev-events-popup'
    dPop.innerHTML = result
    container.appendChild(dPop)
    //
    var cOffset = $(container).offset();
    var cLeft = cOffset.left;
    var wWidth = $(window).width();
    var diff = wWidth - (cLeft + 400);
    if(diff < 50){
      dPop.style.marginLeft = diff - 50  + 'px'
    }
    //
  });
}