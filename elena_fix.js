$(function() {

  var els = [
    /*'.slick-track .slick-slide .block-title__white',  '.slick-track .slick-slide .block .block-text__white',  */'.slider-arrows .slider-btn',
    '.block-title-back__pale', '.block-title-back__white',  '.block-title .line',  '.about .block-text',  '.block-text',  '.about .block-title-back',  '.about-stat-ic',
    '.about-stat-number',  '.about-stat-title',  '.about-stat-description',  '.about-stat-rezults',  '.national-segments .block-subtitle',
    '.national-segments .national-segments-scheme',  '.btn-dark',  '.national-segments .block-text', '.about-stat-rezults', '.about-stat-rezults img'
  ];

  for (var i = 0; i < els.length; i++) {
    var el = $(els[i]);
    $(els[i]).inViewport(function(px) {
      if (px > 10) {
        $(this).addClass('triggered-animate');
      } else {
        // repeat animate?
      }
    });
  }

  $('a.btn-dark').hover(function() {
    $(this).removeClass('triggered-animate');
  })


});
