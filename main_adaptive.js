function checkMenu() {
  if ($(window).width() > 480) {
    $('.eaunion-menu-dropdown').hide()
    $('#menu-top').addClass('menu-desktop').removeClass('mobile').show()
  } else {
    $('.eaunion-menu-dropdown').show()
    $('#menu-top').removeClass('menu-desktop').addClass('mobile').hide()
  }
}

$(window).resize(function () {
  setTimeout(function () {
    checkMenu();
  }, 100);
});

$(document).ready(function () {
  initEvents()
  $('.eaunion-menu-dropdown').click(function (e) {
    $('#menu-top').removeClass('menu-desktop').addClass('mobile').toggle()
  });
  initBtnUp()
})


function initEvents() {
  $('body').click(function (evt) {
    clearDevEvents()
    hideMainMenu(evt)
  })
}

function clearDevEvents() {
  $('.dev-events-popup').remove()
}

function hideMainMenu(evt) {
  //nav#menu
  // label#burger
//input#menuAvPaa//
  // TODO
  var t = evt.target
  if ($(t).closest('nav#menu').length == 0 && t.id !== 'burger' && t.id !== 'menuAvPaa') {
    if (document.getElementById('menuAvPaa').checked == true) {
      document.getElementById('menuAvPaa').checked = false
    }
  }
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function scrollToActiveAccordionHeader(accordionHeader) {
  $('html, body').animate({
    scrollTop: accordionHeader.offset().top
  }, 500);
}

function fixDialogPosition(container) {
  var dlg = $(container).closest('.ui-dialog')
  dlg.focus()
  dlg.position({my: "center center", at: "center center", of: $(window)})
}


function initBtnUp() {
  var btn = $('#button');
  $(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });
  btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({scrollTop: 0}, '300');
  });
}



function isIE() {
  return detectIE() != false
}

function detectIE() {
  var ua = window.navigator.userAgent;

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}